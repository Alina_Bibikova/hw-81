const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UrlShema = new Schema({
    shortUrl: {
        type: String, required: true, unique: true
    },
    originalUrl: {
        type: String, required: true
    }
});

const Url = mongoose.model('Url', UrlShema);

module.exports = Url;