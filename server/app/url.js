const express = require('express');
const nanoid = require('nanoid');
const Url = require('../modals/Url');
const router = express.Router();

router.get('/', (req, res) => {
    Url.find()
        .then(links => res.send(links))
        .catch(() => res.sendStatus(500));
});

router.get('/:shortUrl', (req, res) => {
    Url.findOne({shortUrl: req.params.shortUrl})
        .then(result => {
            if (result) return res.status(301).redirect(result.originalUrl);
            res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', (req, res) => {
    const UrlData = req.body;

    if(UrlData.originalUrl === '') {
        return res.status(400).send({error: "Url is required!"});
    } else {
        UrlData.shortUrl = nanoid(8);
        const links = new Url(UrlData);

        links.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    }
});

module.exports = router;