const express = require('express');
const url = require('./app/url');
const mongoose = require('mongoose');
const app = express();

const cors = require('cors');

app.use(express.json());
app.use(cors());

const port = 8000;

mongoose.connect('mongodb://localhost:27017/homework-81', {userNewUrlParser: true}).then(() => {
    app.use('/url', url);
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});


