import React from 'react';

const Url = props => {
    return (
        <div>
            <h3>Your link now looks like this:</h3>
            <a href={props.original} target='blank'>{props.url}</a>
        </div>
    );
};

export default Url;