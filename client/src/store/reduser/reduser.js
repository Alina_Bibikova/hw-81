import {CREATE_ERROR_FAILURE, CREATE_URL_SUCCESS} from "../action/actionTypes";

const initialState = {
    url: null,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_URL_SUCCESS:
            return {...state,
                url: action.data
            };

        case CREATE_ERROR_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error.response.data.error
            };

        default:
            return state;
    }
};

export default reducer;