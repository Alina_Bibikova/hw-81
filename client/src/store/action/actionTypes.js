import axios from '../../axios-api';

export const CREATE_URL_SUCCESS = 'CREATE_URL_SUCCESS';
export const CREATE_ERROR_FAILURE = 'CREATE_MESSAGE_FAILURE';
export const createErrorFailure = error => ({type: CREATE_ERROR_FAILURE, error});
export const createUrlSuccess = data => ({type: CREATE_URL_SUCCESS, data});

export const fetchUrl = (url) => {
    return () => {
        return axios.get(`/url/${url}`);
    };
};

export const createUrl = link => {
    return dispatch => {
        return axios.post('/url', link).then(
            (response) => dispatch(createUrlSuccess(response.data)),
                error => dispatch(createErrorFailure(error))
        );
    };
};