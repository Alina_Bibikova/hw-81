import React, { Component } from 'react';
import './App.css';
import Url from "./components/Url/Url";
import {createUrl} from "./store/action/actionTypes";
import {connect} from "react-redux";

class App extends Component {
    state = {
        originalUrl: '',
    };

    clickHandler = () => {
       if (this.state.originalUrl !== '') {
            this.props.addUrl(this.state);
            this.setState({
                originalUrl: '',
            })
       } else {
           alert('Fill the field!');
       }
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

  render() {

    return (
      <div className="container">
          {/*{this.props.error*/}
              {/*? <p style={{color: 'red'}}>{this.props.error}</p>*/}
              {/*: null*/}
          {/*}*/}
          <div>
              <h1>Shorten your link!</h1>
              <input
                     type="text" required
                     name="originalUrl"
                     placeholder="Enter URL here"
                     value={this.state.originalUrl}
                     onChange={this.inputChangeHandler}
              />
              <button onClick={this.clickHandler}>Shorten</button>
          </div>
          {this.props.url ? <Url original={this.props.url.originalUrl} url={this.props.url.shortUrl}/> : null}
      </div>
    );
  }
}

const mapStateToProps = state => ({
    url: state.url,
    error: state.error,
});

const mapDispatchToProps = dispatch => ({
    addUrl: link => dispatch(createUrl(link))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
